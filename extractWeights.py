from scipy import io as sio
import numpy as np
import pickle

matfile = sio.loadmat('./imagenet-vgg-verydeep-19.mat')
layers = matfile['layers'][0]
layers_list = []
for items in layers:
    name = items[0][0][0][0]
    if 'conv' in name:
        weights = np.single(items[0][0][4].transpose(3,2,0,1))
        bias = np.single(np.squeeze(items[0][0][3]))
        layers_list.append({'name':name, 'weights':weights, 'bias':bias})
    if 'fc6' in name:
        weights = np.single(items[0][0][4].transpose(3,2,0,1).reshape(4096, -1))
        bias = np.single(np.squeeze(items[0][0][3]))
        layers_list.append({'name':name, 'weights':weights, 'bias':bias})
    elif 'fc' in name:
        weights = np.single(np.squeeze(items[0][0][4].T))
        bias = np.single(np.squeeze(items[0][0][3]))
        layers_list.append({'name':name, 'weights':weights, 'bias':bias})


with file('vgg19-weights.pkl','wb') as fp:
    pickle.dump(layers_list, fp)

