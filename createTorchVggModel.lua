require 'torch'
require 'nn'

torch.setdefaulttensortype('torch.FloatTensor')

local cmd = torch.CmdLine()
cmd:text()
cmd:text('Build VGG torch model')
cmd:text()
cmd:text('Options:')
------------- Config options --------------------
cmd:option('-conv_backend', 'caffe', 'caffe/cudnn/fbfft')
cmd:option('-weights_file', './vgg19-weights.t7', 'file contains weights')

local opt = cmd:parse(arg)
print(opt)

local layers_weights = torch.load(opt.weights_file)
local layer_counter = 1

if opt.conv_backend == 'caffe' then
    SpatialMaxPooling = nn.SpatialMaxPooling
    SpatialConvolution = nn.SpatialConvolutionMM
    ReLU = nn.ReLU
elseif opt.conv_backend == 'cudnn' then
    require 'cudnn'
    SpatialMaxPooling = cudnn.SpatialMaxPooling
    SpatialConvolution = cudnn.SpatialConvolution
    ReLU = cudnn.ReLU
end

local vgg_net = nn.Sequential()
local features = nn.Sequential()
cfg = {64, 64, 'M', 128, 128, 'M', 256, 256, 256, 256, 'M', 512, 512, 512, 512, 'M', 512, 512, 512, 512, 'M'}
local iChannels = 3;
for k,v in ipairs(cfg) do
 if v == 'M' then
    features:add(SpatialMaxPooling(2,2,2,2))
 else
    local oChannels = v
    local conv3 = SpatialConvolution(iChannels,oChannels,3,3,1,1,1,1)

    conv3.weight:copy(layers_weights[layer_counter].weights)
    conv3.bias:copy(layers_weights[layer_counter].bias)
    layer_counter = layer_counter + 1

    features:add(conv3)
    features:add(ReLU(true))
    iChannels = oChannels
 end
end

local classifier = nn.Sequential()
classifier:add(nn.View(512*7*7))
local linear = nn.Linear(512*7*7, 4096)
linear.weight:copy(layers_weights[layer_counter].weights)
linear.bias:copy(layers_weights[layer_counter].bias)
layer_counter = layer_counter + 1
classifier:add(linear)
classifier:add(ReLU(true))
classifier:add(nn.Dropout(0.5))
linear = nn.Linear(4096, 4096)
linear.weight:copy(layers_weights[layer_counter].weights)
linear.bias:copy(layers_weights[layer_counter].bias)
layer_counter = layer_counter + 1
classifier:add(linear)
classifier:add(ReLU(true))
classifier:add(nn.Dropout(0.5))
linear = nn.Linear(4096, 1000)
linear.weight:copy(layers_weights[layer_counter].weights)
linear.bias:copy(layers_weights[layer_counter].bias)
layer_counter = layer_counter + 1
classifier:add(linear)
classifier:add(nn.LogSoftMax())

vgg_net:add(features):add(classifier)
collectgarbage()
print(vgg_net)
torch.save('vgg-net.t7',vgg_net)

