require 'torch'
py = require 'fb.python'

py.exec('import pickle;import numpy as np')
py.exec("fp = open('./vgg19-weights.pkl','rb')")
py.exec("layers = pickle.load(fp); fp.close()")

layers = {}
nLayers = py.eval('len(layers)')

for i=1, nLayers do
    layer = {}
    layer.name = py.eval("str(layers["..(i-1).."]['name'])")
    layer.weights = py.eval("layers["..(i-1).."]['weights']"):clone()
    layer.bias = py.eval("layers["..(i-1).."]['bias']"):clone()
    layers[#layers+1] = layer
end

torch.save('vgg19-weights.t7',layers)
